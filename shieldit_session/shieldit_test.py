import unittest
import traceback
import time
from tools import HTMLTestRunner
import logging
from Tests.log_in_system.login import LoginTest
from Tests.log_in_system.auto_login import AutoLoginTest
from Tests.call_system.call_android import CallTestAndroid
from Tests.call_system.call_ios import CallTestIos
from Tests.call_system.incoming_call_android import IncomingCallAndroid
from Tests.network_changes.network_change import NetworkChanges
from Tests.chat_system.message_android import MessageTestAndroid
from Tests.chat_system.message_ios import MessageTestIos
from Tests.chat_system.receive_message_android import MessageReceivedTestAndroid
from Tests.group_chat.group_chats import GroupChatTest
from Tests.call_system.in_call_android import InCallTestAndroid
from Tests.notifications.receive_notification import ReceiveNotificationsTestAndroid


def runTest():
  times = 0
  timeout = time.time() + 60*1  # 24h = 60*60*24
  timestr = time.strftime('%Y_%m_%d_%H.%M.%S', time.localtime(time.time()))

  filename = "./logs/"+timestr+".html"
  with open(filename, 'wb') as f:
    runner = HTMLTestRunner.HTMLTestRunner(
    stream=f,
    title=u'Test Report: {0}'.format(times),
    description=u'24 test case, test by TG'
    )
    logging.info('Test Times: {0}'.format(times))
    times += 1
    runner.run(suite())



# must do before running rules:
# test git
def suite():
  suite = unittest.TestSuite()
  tests = [
    # login tests
    # LoginTest('test_login'),
    # AutoLoginTest('test_auto_login'),
    #
    # call test Android
    CallTestAndroid('test_hangup_call_to_android'),
    CallTestAndroid('test_call_to_android'),
    CallTestAndroid('test_call_to_android_from_chat'),
    CallTestAndroid('test_call_to_android_from_contact'),
    CallTestAndroid('test_call_to_android_from_plus_chat'),
    CallTestAndroid('test_call_to_android_from_recent_calls'),

    # receive call test Android
    IncomingCallAndroid('test_receive_incoming_call_from_android'),
    IncomingCallAndroid('test_receive_incoming_hangup_call_from_android'),
    IncomingCallAndroid('test_receive_incoming_call_while_app_closed_from_android'),

    # in call test Android
    InCallTestAndroid('test_add_call_from_dialer_android'),
    InCallTestAndroid('test_add_call_from_contacts_android'),
    InCallTestAndroid('test_add_call_from_recent_calls_android'),

    # call test iOS
    CallTestIos('test_call_to_ios'),
    CallTestIos('test_hangup_call_to_ios'),
    CallTestIos('test_call_to_ios_from_chat'),
    CallTestIos('test_call_to_ios_from_plus_chat'),
    CallTestIos('test_call_to_ios_from_contact'),
    CallTestIos('test_call_to_ios_from_recent_calls'),

    # receive notification
    ReceiveNotificationsTestAndroid('test_receive_message_notification_android'),
    ReceiveNotificationsTestAndroid('test_receive_missed_call_notification_android'),
    ReceiveNotificationsTestAndroid('test_receive_attachment_notification_android'),
    ReceiveNotificationsTestAndroid('test_added_to_group_chat_notification_android'),
    ReceiveNotificationsTestAndroid('test_receive_message_notification_from_group_chat_android'),
    ReceiveNotificationsTestAndroid('test_receive_kicked_out_of_group_notification_android'),

    # message test android
    MessageTestAndroid('test_send_msg_from_contacts_to_android'),
    MessageTestAndroid('test_send_msg_from_chat_to_android'),
    MessageTestAndroid('test_send_msg_from_plus_in_chat_to_android'),
    MessageTestAndroid('test_send_attachment_to_android'),
    # MessageTestAndroid('test_resend_msg_to_android'),
    MessageTestAndroid('test_send_msg_from_recent_calls_to_android'),
    MessageTestAndroid('test_send_read_receipt_msg_android'),

    # message received Android
    MessageReceivedTestAndroid('test_received_message_from_android'),
    MessageReceivedTestAndroid('test_receive_attachment_from_android'),

    # message test iOS
    MessageTestIos('test_send_msg_from_contacts_to_ios'),
    MessageTestIos('test_send_msg_from_chat_to_ios'),
    MessageTestIos('test_send_msg_from_plus_in_chat_to_ios'),
    MessageTestIos('test_send_attachment_to_ios'),
    MessageTestIos('test_send_msg_from_recent_calls_to_ios'),

    # group chat test
    GroupChatTest('test_create_group'),
    GroupChatTest('test_send_attachment_in_group'),
    GroupChatTest('test_send_msg_in_group_chat'),
    GroupChatTest('test_mute_group_chat'),
    GroupChatTest('test_send_call_from_group_chat'),
    GroupChatTest('test_send_msg_from_group_chat'),
  ]

  suite.addTests(tests)
  return suite


if __name__ == "__main__":
  try:
    runTest()
  except Exception as e:
    logging.info('Exception: {0}'.format(e))
    logging.debug('Exception: {0}'.format(traceback.format_exc()))
  finally:
    print('ShieldIT Test Reports.')