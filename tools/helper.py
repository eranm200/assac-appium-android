import subprocess
import time
from appium.webdriver.common.touch_action import TouchAction
import unittest
from appium import webdriver
from threading import Thread
import smtplib


# Setup
desired_caps_xiaomi = {
    'platformName': 'Android',
    'platformVersion': '9',
    'automationName': 'UiAutomator1',
    'deviceName': 'Xiaomi Redmi Note 6 Pro API 27',
    'appPackage': 'com.assacnetworks.lac',
    'appActivity': 'com.assacnetworks.lac.presentation.splash.SplashActivity',
    'autoGrantPermissions': True,
    'udid': '9e73e2d',
    'deviceid': '192.168.1.170',
    # 'deviceid': '9e73e2d',
    # 'deviceid': '9e73e2d',
    'noReset': True,
    'fullReset': False,
    'newCommandTimeout': '4000',
    'adbExecTimeout': '80000',
    'systemPort': '5000'
}

desired_caps_a51 = {
    'platformName': 'Android',
    'platformVersion': '10',
    'automationName': 'UiAutomator1',
    'deviceName': 'Galaxy A51',
    'appPackage': 'com.assacnetworks.shieldit',
    'appActivity': 'com.assacnetworks.shieldit.presentation.splash.SplashActivity',
    'autoGrantPermissions': True,
    'deviceid': '210.210.254.112',
    # 'deviceid': '9e73e2d',
    # 'deviceid': '9e73e2d',
    'noReset': True,
    'fullReset': False,
    'newCommandTimeout': '4000',
    'adbExecTimeout': '80000',
    'systemPort': '5000'
}

# Setup OnePlus 6
desired_caps_one_plus = {
    'platformName': 'Android',
    'platformVersion': '9',
    'automationName': 'UiAutomator1',
    'deviceName': 'Galaxy S8',
    'appPackage': 'com.assacnetworks.shieldit',
    'appActivity': 'com.assacnetworks.shieldit.presentation.splash.SplashActivity',
    'autoGrantPermissions': True,
    'deviceid': '210.210.254.121',
    # 'deviceid': '9e73e2d',
    'noReset': True,
    'fullReset': False,
    'newCommandTimeout': '4000',
    'adbExecTimeout': '80000',
    'systemPort': '5000'
}

# Setup OnePlus with noReset: False
desired_caps_one_plus_no_reset_false = {
    'platformName': 'Android',
    'platformVersion': '9',
    'deviceName': 'one plus 6',
    'automationName': 'UiAutomator1',
    'appPackage': 'com.assacnetworks.shieldit',
    'appActivity': 'com.assacnetworks.shieldit.presentation.splash.SplashActivity',
    'app': 'D:\\APK\\app-release.apk',
    'autoGrantPermissions': True,
    'noReset': False,
    'fullReset': False
   }

# Setup Xiaomi Redmi Note 6 Pro
desired_caps_Xiaomi__no_reset_false = {
   'platformName': 'Android',
   'platformVersion': '8.1',
   'automationName': 'UiAutomator1',
   'deviceName': 'Xiaomi Redmi Note 6 Pro API 27',
   'appPackage': 'com.assacnetworks.shieldit',
   'appActivity': 'com.assacnetworks.shieldit.presentation.splash.SplashActivity',
   'autoGrantPermissions': True,
   'noReset': True,
   'fullReset': False
   }


# Setup Xiaomi Redmi note 6 pro Noreset False
desired_capsXiaomiF = {
   'platformName': 'Android',
   'platformVersion': '8.1',
   'automationName': 'UiAutomator1',
   'deviceName': 'Xiaomi Redmi Note 6 Pro API 27',
   'appPackage': 'com.assacnetworks.shieldit',
   'appActivity': 'com.assacnetworks.shieldit.presentation.splash.SplashActivity',
   'autoGrantPermissions': True,
   'noReset': False,
   'fullReset': False
   }


# Samsung galaxy s8 Galaxy S8,ce11182b3955861b02 3988
desired_caps_Samsung = {
    'platformName': 'Android',
    'platformVersion': '9',
    'automationName': 'UiAutomator1',
    'deviceName': 'Galaxy S8',
    'appPackage': 'com.assacnetworks.lac',
    'appActivity': 'com.assacnetworks.lac.presentation.splash.SplashActivity',
    'autoGrantPermissions': True,
    'udid': 'ce11182b3955861b02',
    # 'deviceid': '192.168.1.148',
    # 'deviceid': '210.210.254.114',
    # 'deviceid': '42005518ce9ac4cf',
    'noReset': True,
    'fullReset': False,
    'newCommandTimeout': '4000',
    'adbExecTimeout': '80000',
    'systemPort': '5000'
}

# Samsung galaxy s8
desired_capsSamsungFalse = {
    'platformName': 'Android',
    'platformVersion': '9',
    'automationName': 'UiAutomator1',
    'deviceName': 'Galaxy S8',
    'appPackage': 'com.assacnetworks.shieldit',
    'appActivity': 'com.assacnetworks.shieldit.presentation.splash.SplashActivity',
    'udid': 'ce11182b3955861b02',
    'autoGrantPermissions': True,
    'noReset': False,
    'fullReset': False
}

desired_capsA10 = {
    'platformName': 'Android',
    'platformVersion': '9',
    'automationName': 'UiAutomator1',
    'deviceName': 'Galaxy A10',
    'appPackage': 'com.assacnetworks.shieldit',
    'appActivity': 'com.assacnetworks.shieldit.presentation.splash.SplashActivity',
    'autoGrantPermissions': True,
    'noReset': True,
    'fullReset': False
}

desired_capsJ3 = {
    'platformName': 'Android',
    'platformVersion': '9',
    'automationName': 'UiAutomator1',
    'deviceName': 'Galaxy J3 Pro',
    'appPackage': 'com.assacnetworks.shieldit',
    'appActivity': 'com.assacnetworks.shieldit.presentation.splash.SplashActivity',
    'autoGrantPermissions': True,
    'deviceid': '210.210.254.123',
    # 'deviceid': '9e73e2d',
    'noReset': True,
    'fullReset': False,
    'newCommandTimeout': '4000',
    'adbExecTimeout': '80000',
    'systemPort': '5000'
}

desired_caps = desired_caps_xiaomi
secondary_desired_caps = desired_caps_Samsung

# //////// Port /////////
main_port = 'http://127.0.0.1:4723/wd/hub'
second_port = 'http://127.0.0.1:4725/wd/hub'


# //////// login/out/auto/////////
def old_log_in(driver, extension, password):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/extension_ET').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/extension_ET').send_keys(extension)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/password_ET').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/password_ET').send_keys(password)
    driver_back(driver)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/login_btn').click()


def log_in(driver, phone_number):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/phone_number_ET').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/phone_number_ET').send_keys(phone_number)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/checkbox').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/assac_logo').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/continueBtn').click()
    time.sleep(2)
    driver.swipe(start_x=948, start_y=55, end_x=1006, end_y=1027)
    element = driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView').text
    x = element.split("Your verification code is: " )
    print(x)
    time.sleep(5)
    driver_back(driver)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/code_ET').send_keys(x)


def log_out(driver):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/profile_ic').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/btn_logout').click()
    time.sleep(2)
    driver.find_element_by_id('android:id/button1').click()


def get_text(driver):
    driver.swipe(start_x=948, start_y=55, end_x=1006, end_y=1027)
    time.sleep(5)
    element = driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView').text
    x = element.split("Your verification code is: ",)
    print(x)
    time.sleep(4)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/code_ET').send_keys(x)


# ////////sleep////////
def sleep_until_app_logged_in():
    time.sleep(10)


def sleep_call_connecting():
    time.sleep(5)


def sleep_Call_disconnected():
    time.sleep(5)


def sleep_msg_sent_to_android():
    time.sleep(5)


def sleep_msg_sent_to_ios():
    time.sleep(7)


def sleep_msg_received_android():
    time.sleep(12)


def sleep_group_chat_created():
    time.sleep(4)


def sleep_group_chat_mute():
    time.sleep(5)


# ////////assert////////
def assertion_by_line(driver, line):
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    print('\nal;all lines of second log call: ')
    print('\n'.join(second_set_of_log_messages[:]))
    for log in second_set_of_log_messages:
        if line in log:
            is_registration_success = True
            return is_registration_success


def assert_for_network_change(driver):
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    print('\nal;all lines of second log call: ')
    print('\n'.join(second_set_of_log_messages[:]))
    for log in second_set_of_log_messages:
        if 'Returned to call after network change' in log:
            handleip_success = True
            return handleip_success


def assertion_for_message_lost(driver):
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    print('\nal;all lines of second log call: ')
    print('\n'.join(second_set_of_log_messages[:]))
    for log in second_set_of_log_messages:
        if 'Message sent to an ios device' in log:
            msg_sent_ios = True
            return msg_sent_ios


def assertion_for_received_msg_ios(driver):
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    for log in second_set_of_log_messages:
        if 'Message received from an ios device' in log:
            call_incoming = True
            return call_incoming


def assertion_for_message_received_notification(driver):
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    print('\nal;all lines of second log call: ')
    print('\n'.join(second_set_of_log_messages[:]))
    for log in second_set_of_log_messages:
        if 'Notification: message was received' in log:
            main_screen = True
            return main_screen


def assertion_for_missed_call_notification(driver):
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    print('\nal;all lines of second log call: ')
    print('\n'.join(second_set_of_log_messages[:]))
    for log in second_set_of_log_messages:
        if 'Notification: missed call' in log:
            main_screen = True
            return main_screen


def assertion_for_added_to_group(driver):
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    print('\nal;all lines of second log call: ')
    print('\n'.join(second_set_of_log_messages[:]))
    for log in second_set_of_log_messages:
        if 'Notification: You have been added to group chat' in log:
            main_screen = True
            return main_screen


# ////////log functions////////
def clear_logs(driver):
    subprocess.call('adb -s ce11182b3955861b02 shell logcat -b all -c', shell=True)
    subprocess.call('adb -s 9e73e2d shell logcat -b all -c', shell=True)

def get_first_log(driver):
    logs = driver.get_log('logcat')
    log_messages = list(map(lambda log: log['message'], logs))


def get_second_log(driver):
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))


def print_first_log(driver):
    logs = driver.get_log('logcat')
    log_messages = list(map(lambda log: log['message'], logs))
    print('\nal;all lines of second log call: ')
    print('\n'.join(log_messages[:]))


def print_second_log(driver):
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    print('\nal;all lines of second log call: ')
    print('\n'.join(second_set_of_log_messages[:]))


def log_listen_incoming_call(driver):
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    for log in second_set_of_log_messages:
        while 'Call incoming' in log:
            incoming_call = True
            return incoming_call
        else:
            return time.sleep(20)


# ////////driver functions////////
def answer_call(driver):
    time.sleep(3)
    logs = driver.get_log('logcat')
    second_set_of_log_messages = list(map(lambda log: log['message'], logs))
    print('\nal;all lines of second log call: ')
    print('\n'.join(second_set_of_log_messages[:]))
    for log in second_set_of_log_messages:
        if 'Call incoming' in log:
            time.sleep(1)
            return driver.find_element_by_id('com.assacnetworks.shieldit:id/answerCall').click()


def hang_up(driver):
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/hangUpBtn').click()


def swipe_to_refresh(driver):
    driver.swipe(start_x=500, start_y=900, end_x=478, end_y=1200)


def driver_reset(driver):
    driver.reset()


def driver_open(driver):
    driver.launch_app()


def driver_close(driver):
    driver.close_app()


def driver_rec_start(driver):
    driver.find_element_by_id('com.hecorat.screenrecorder.free:id/btn_record').click()
    time.sleep(1)
    driver.find_element_by_id('android:id/button1').click()


def driver_rec_open(driver):
    driver.swipe(start_x=948, start_y=55, end_x=1006, end_y=1027)


def driver_rec_stop1(driver):
    driver.swipe(start_x=863, start_y=173, end_x=863, end_y=173)


def driver_rec_stop2(driver):
    driver.swipe(start_x=976, start_y=173, end_x=976, end_y=173)


def driver_rec_swipe(driver):
    driver.swipe(start_x=676, start_y=1652, end_x=976, end_y=2052)


def driver_rec_swipe_left(driver):
    driver.swipe(start_x=976, start_y=2052, end_x=133, end_y=2047)


def driver_notif_center(driver):
    driver.swipe(start_x=567, start_y=39, end_x=542, end_y=1267)


def driver_lock(driver):
    driver.lock()


def driver_press_speaker(driver):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/speakerBtn').click()


def driver_press_hold(driver):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/holdBtn').click()


def driver_press_mute(driver):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/muteBtn').click()


def driver_back(driver):
    driver.back()
    # driver.press_keycode(4)


# //////// message from android to iOS////////
def send_message_from_contacts_ios(driver, contact):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(3)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from contacts')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()


def send_message_from_plus_chat_ios(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/plusBtn').click()
    time.sleep(3)
    driver.swipe(start_x=545, start_y=1748, end_x=545, end_y=1748)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageBtn').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from + in chat')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()


def send_message_chat_ios(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    check_if_assac_number(driver)
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageBtn').click()
    time.sleep(4)
    driver_back(driver)
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from chat')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()


def send_attachmant_ios(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(2)
    driver.swipe(start_x=922, start_y=779, end_x=922, end_y=779)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/attachmentButton').click()
    time.sleep(3)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.ImageView').click()
    time.sleep(2)
    driver.find_element_by_id('com.android.documentsui:id/menu_open').click()


def resend_msg_ios(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(3)
    driver.swipe(start_x=500, start_y=900, end_x=478, end_y=1200)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageBtn').click()
    toggle_wifi(driver)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from contacts')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()
    actions = TouchAction(driver)
    time.sleep(4)
    actions.long_press(x=824, y=1198)
    actions.perform()
    toggle_wifi(driver)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/resend_btn').click()
    driver.find_element_by_id('android:id/button1').click()


def send_message_from_recent_calls_ios(driver):
    driver.find_element_by_accessibility_id('Recent calls').click()
    time.sleep(1)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageBtn').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from recent calls')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()


# //////// call from dialer////////
def call_from_dialer_8899(driver):
        driver.find_element_by_id('com.assacnetworks.shieldit:id/eight').click()
        driver.find_element_by_id('com.assacnetworks.shieldit:id/eight').click()
        driver.find_element_by_id('com.assacnetworks.shieldit:id/nine').click()
        driver.find_element_by_id('com.assacnetworks.shieldit:id/nine').click()
        time.sleep(2)
        driver.find_element_by_id('com.assacnetworks.shieldit:id/call_ic').click()


def call_from_dialer_1002(driver):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/one').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/zero').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/zero').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/two').click()
    time.sleep(4)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_ic').click()
    time.sleep(2)


def call_from_dialer_9042(driver):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/nine').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/zero').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/four').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/two').click()
    time.sleep(4)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_ic').click()


def call_from_dialer_1006(driver):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/one').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/zero').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/zero').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/six').click()
    time.sleep(4)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_ic').click()
    time.sleep(2)


# ////////call test iOS////////
def call_from_plus_chat_ios(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/plusBtn').click()
    time.sleep(2)
    driver.swipe(start_x=545, start_y=1748, end_x=545, end_y=1748)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageBtn').click()
    time.sleep(2)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_btn').click()


def call_from_chat_ios(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(3)
    driver.swipe(start_x=500, start_y=900, end_x=478, end_y=1200)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageBtn').click()
    time.sleep(3)
    driver_back(driver)
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    time.sleep(4)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_btn').click()


def call_from_contact_ios(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(3)
    driver.swipe(start_x=500, start_y=900, end_x=478, end_y=1200)
    time.sleep(1)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageBtn').click()
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_btn').click()


def call_from_recent_calls_ios(driver):
    driver.find_element_by_accessibility_id('Recent calls').click()
    time.sleep(2)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/callBtn').click()


# //////// message functions android//////
def send_message_from_contacts_android(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(4)
    check_if_assac_number(driver)
    time.sleep(3)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from contacts')
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()


def send_message_from_plus_chat_android(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/plusBtn').click()
    time.sleep(2)
    driver.swipe(start_x=545, start_y=1748, end_x=545, end_y=1748)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from + in chat')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()


def send_message_from_chat_android(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    check_if_assac_number(driver)
    check_if_assac_number(driver)
    time.sleep(3)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    time.sleep(4)
    driver_back(driver)
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from chat')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()


# ///// put actions in a function
def resend_msg_android(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(3)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    toggle_wifi(driver)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from contacts')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()
    toggle_wifi(driver)
    time.sleep(4)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/failed_message_info').click()
    time.sleep(4)
    driver.swipe(start_x=552, start_y=1884, end_x=552, end_y=1884)


def send_attachment_android(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/attachmentButton').click()
    time.sleep(4)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.ImageView').click()
    time.sleep(2)
    driver.find_element_by_id('com.android.documentsui:id/menu_open').click()


def send_attachment_android_s8(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/attachmentButton').click()
    time.sleep(4)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]').click()


def send_message_from_recent_calls_android(driver):
    driver.find_element_by_accessibility_id('Recent calls').click()
    time.sleep(1)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageBtn').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from recent calls')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()


def send_message_from_chat_to_xiaomi(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    time.sleep(3)
    driver_back(driver)
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    time.sleep(4)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from chat')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()


def send_attachment_from_chat_to_xiaomi(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/attachmentButton').click()
    time.sleep(4)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ImageView').click()
    time.sleep(2)
    driver.find_element_by_id('com.android.documentsui:id/menu_open').click()


def delete_message_from_chat_android(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from chat')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()
    actions = TouchAction(driver)
    actions.long_press(driver.find_element_by_id('com.assacnetworks.shieldit:id/container'))
    actions.perform()
    time.sleep(1)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/trash_btn').click()
    time.sleep(3)
    driver.find_element_by_id('android:id/button1').click()


def delete_attachment_from_chat_android(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/attachmentButton').click()
    time.sleep(4)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.ImageView').click()
    time.sleep(2)
    driver.find_element_by_id('com.android.documentsui:id/menu_open').click()
    actions = TouchAction(driver)
    actions.long_press(driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.View'))
    actions.perform()
    time.sleep(1)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/trash_btn').click()
    driver.find_element_by_id('android:id/button1').click()


# ////////GROUP CHAT/////////
def create_group_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    time.sleep(2)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/plusBtn').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Button[2]').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView').click()
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView').click()
    time.sleep(2)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/nextBtn').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/name_input').send_keys('shmerling')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/create').click()


def send_message_in_group_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi group')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()


def send_attachment_in_group_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/attachmentButton').click()
    time.sleep(3)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.ImageView').click()
    time.sleep(2)
    driver.find_element_by_id('com.android.documentsui:id/menu_open').click()


def add_contacts_in_group_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/info_btn').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[3]/android.view.ViewGroup').click()
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView').click()
    driver.swipe(start_x=932, start_y=1307, end_x=932, end_y=1307)


def mute_group_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/info_btn').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[4]/android.view.ViewGroup').click()
    driver.find_element_by_id('android:id/button1').click()


def leave_group_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/info_btn').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/leave_group_btn').click()


def call_contact_from_group_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/info_btn').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup').click()
    time.sleep(2)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_contact_btn').click()


def message_from_group_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/info_btn').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup').click()
    time.sleep(2)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/chat_btn').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageInput').send_keys('hi from shmreling chat')
    driver.find_element_by_id('com.assacnetworks.shieldit:id/messageSendButton').click()

def admin_remove_member_from_group_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/info_btn').click()
    time.sleep(2)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup').click()
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/kick_btn').click()
    time.sleep(2)
    driver.find_element_by_id('android:id/button1').click()


# ///////call test android////////
def call_from_plus_chat_android(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/plusBtn').click()
    time.sleep(3)
    driver.swipe(start_x=545, start_y=1748, end_x=545, end_y=1748)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    time.sleep(3)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_btn').click()
    time.sleep(3)


def call_from_chat_android(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    check_if_assac_number(driver)
    check_if_assac_number(driver)
    time.sleep(3)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    time.sleep(3)
    driver_back(driver)
    driver.find_element_by_accessibility_id('Chats').click()
    time.sleep(2)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()
    time.sleep(5)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_btn').click()
    time.sleep(3)


def call_from_contacts_android(driver):
    driver.find_element_by_accessibility_id('Contacts').click()
    time.sleep(3)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').click()
    time.sleep(5)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_btn').click()
    time.sleep(3)


def call_from_recent_calls_android(driver):
    driver.find_element_by_accessibility_id('Recent calls').click()
    time.sleep(1)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/callBtn').click()


# ///////in call test android////////
def add_call(driver):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/addCallBtn').click()


def in_call_add_from_contacts(driver):
    add_call(driver)
    driver.find_element_by_accessibility_id('Contacts').click()
    check_if_assac_number(driver)
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.view.ViewGroup').click()
    driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.Button[2]').click()


def in_call_add_from_dialer(driver):
    add_call(driver)
    call_from_dialer_1002(driver)


def in_call_add_from_recent_calls(driver):
    add_call(driver)
    driver.find_element_by_accessibility_id('Recent calls').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/callBtn').click()


# /////// SIMPLE FUNCTIONS /////////
def go_to_chat(driver):
    driver.find_element_by_accessibility_id('Chats').click()


def go_to_chat_first_conversation(driver):
    driver.find_element_by_accessibility_id('Chats').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/dialogContainer').click()


def go_to_shield(driver):
    driver.find_element_by_accessibility_id('Shield').click()


# ///////android device settings toggles///////
def toggle_wifi(driver):
    driver.toggle_wifi()


# ///////assac number refresh/////////
def check_if_assac_number(driver):
    try:
        while driver.find_element_by_xpath('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.LinearLayout/android.widget.ImageView').is_displayed():
            isassac = True
            return isassac
    except:
        print('no element')
        return driver.swipe(start_x=500, start_y=900, end_x=478, end_y=1200)


# ///////shieldit safety functions/////////
def scan_apps(driver):
    go_to_shield(driver)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/apps_safety_btn').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/scan_btn').click()




