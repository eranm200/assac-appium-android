from tools.helper import *


class CallTestAndroid(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver2 = webdriver.Remote(second_port, secondary_desired_caps)
        self.driver.implicitly_wait(8)

    def test_call_to_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        time.sleep(10)
        Thread(target=call_from_dialer_1002(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        sleep_call_connecting()
        hang_up(self.driver)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_hangup_call_to_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        time.sleep(10)
        Thread(target=call_from_dialer_1002(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        sleep_Call_disconnected()
        hang_up(self.driver)
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver, 'Call disconnected'))
        time.sleep(3)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_incoming_call_from_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        time.sleep(10)
        Thread(target=call_from_dialer_1002(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver2, 'Call connecting'))
        sleep_call_connecting()
        hang_up(self.driver)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_call_to_android_from_chat(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        time.sleep(10)
        Thread(target=call_from_chat_android(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_call_to_android_from_contact(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        time.sleep(10)
        Thread(target=call_from_contacts_android(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_call_to_android_from_plus_chat(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        time.sleep(10)
        Thread(target=call_from_plus_chat_android(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_call_to_android_from_recent_calls(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        time.sleep(10)
        Thread(target=call_from_dialer_1002(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        hang_up(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        time.sleep(3)
        Thread(target=call_from_recent_calls_android(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        time.sleep(6)
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_press_speaker_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        # clear_logs(self.driver)///need logs
        Thread(target=call_from_dialer_1002(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        driver_press_speaker(self.driver)
        time.sleep(4)
        self.assertTrue(assertion_by_line(self.driver, ''))
        time.sleep(5)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_press_mute_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        # clear_logs(self.driver)///need logs
        call_from_dialer_1002(self.driver)
        answer_call(self.driver2)
        driver_press_mute(self.driver)
        time.sleep(4)
        self.assertTrue(assertion_by_line(self.driver, ''))
        time.sleep(5)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_press_hold_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        # clear_logs(self.driver)///need logs
        call_from_dialer_1002(self.driver)
        answer_call(self.driver2)
        driver_press_hold(self.driver)
        time.sleep(4)
        self.assertTrue(assertion_by_line(self.driver, ''))
        time.sleep(5)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()


if __name__ == '__main__':
    unittest.main()












