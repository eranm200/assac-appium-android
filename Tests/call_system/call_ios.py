from tools.helper import *


# commit ios device integration
class CallTestIos(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver.implicitly_wait(10)

    def test_call_to_ios(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        call_from_dialer_9042(self.driver)
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        driver_close(self.driver)

    def test_hangup_call_to_ios(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        call_from_dialer_9042(self.driver)
        sleep_call_connecting()
        hang_up(self.driver)
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver, 'Call disconnected'))
        driver_close(self.driver)

    # def test_incoming_call_from_ios(self)://///need to connect ios device
    #     driver_open(self.driver)
    #     clear_logs(self.driver)
    #     get_first_log(self.driver)
    #     clear_logs(self.driver)
    #     log_listen_incoming_call(self.driver)
    #     self.assertTrue(assertion_by_line(self.driver, 'call incoming'))
    #     hang_up(self.driver)

    def test_call_to_ios_from_chat(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        call_from_chat_ios(self.driver)
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        driver_close(self.driver)

    def test_call_to_ios_from_plus_chat(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        call_from_plus_chat_ios(self.driver)
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        driver_close(self.driver)

    def test_call_to_ios_from_contact(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        call_from_contact_ios(self.driver)
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        driver_close(self.driver)

    def test_call_to_ios_from_recent_calls(self):
        driver_open(self.driver)
        time.sleep(10)
        call_from_dialer_9042(self.driver)
        hang_up(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        time.sleep(3)
        call_from_recent_calls_ios(self.driver)
        time.sleep(6)
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        driver_close(self.driver)

if __name__ == '__main__':
    unittest.main()












