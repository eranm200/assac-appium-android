from tools.helper import *


class IncomingCallAndroid(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver2 = webdriver.Remote(second_port, secondary_desired_caps)
        self.driver.implicitly_wait(8)

    def test_receive_incoming_call_from_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        sleep_until_app_logged_in()
        clear_logs(self.driver)
        get_first_log(self.driver)
        call_from_dialer_1006(self.driver2)
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver, 'Call incoming'))
        sleep_call_connecting()
        hang_up(self.driver2)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_receive_incoming_hangup_call_from_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        sleep_until_app_logged_in()
        clear_logs(self.driver)
        get_first_log(self.driver)
        Thread(target=call_from_dialer_1006(self.driver2)).start()
        Thread(target=answer_call(self.driver)).start()
        hang_up(self.driver2)
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver, 'Call disconnected'))
        sleep_call_connecting()
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_receive_incoming_call_while_app_closed_from_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        driver_back(self.driver)
        Thread(target=call_from_dialer_1006(self.driver2)).start()
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver, 'Call incoming'))
        sleep_call_connecting()
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

if __name__ == '__main__':
    unittest.main()