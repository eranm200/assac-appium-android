import unittest
import time
from appium import webdriver
from tools.helper import *
from tools.helper import call_from_dialer_9042
from tools.helper import call_from_dialer_1002
from tools.helper import assertion_by_line
from tools.helper import hang_up
from tools.helper import call_from_chat_android
from tools.helper import desired_caps_Samsung
from tools.helper import desired_caps
from tools.helper import answer_call
from tools.helper import get_first_log
from tools.helper import driver_press_hold
from tools.helper import driver_press_mute
from tools.helper import driver_press_speaker
from tools.helper import in_call_add_from_contacts
from tools.helper import in_call_add_from_dialer
from tools.helper import in_call_add_from_recent_calls
from tools.helper import hang_up
from tools.helper import driver_close
from tools.helper import driver_open
from tools.helper import clear_logs
from threading import Thread
from tools.helper import main_port
from tools.helper import second_port


class InCallTestAndroid(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver2 = webdriver.Remote(second_port, secondary_desired_caps)
        self.driver.implicitly_wait(5)

    def test_add_call_from_dialer_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        time.sleep(10)
        call_from_dialer_9042(self.driver)
        clear_logs(self.driver)
        Thread(target=in_call_add_from_dialer(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        time.sleep(4)
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_add_call_from_contacts_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        time.sleep(10)
        call_from_dialer_9042(self.driver)
        clear_logs(self.driver)
        Thread(target=in_call_add_from_contacts(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        time.sleep(4)
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()

    def test_add_call_from_recent_calls_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        time.sleep(10)
        call_from_dialer_9042(self.driver)
        clear_logs(self.driver)
        in_call_add_from_recent_calls(self.driver)
        time.sleep(10)
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        hang_up(self.driver)
        Thread(target=driver_close(self.driver)).start()
        Thread(target=driver_close(self.driver2)).start()


if __name__ == '__main__':
    unittest.main()