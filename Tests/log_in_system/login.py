import unittest
from tools.helper import *

class LoginTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver.implicitly_wait(6)

    def test_login(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        log_in(self.driver, '0547038545')
        sleep_until_app_logged_in()
        self.assertTrue(assertion_by_line(self.driver, "App logged in successfully"))
        driver_close(self.driver)

    def test_logout(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        log_out(self.driver)
        time.sleep(5)
        self.assertTrue(assertion_by_line(self.driver, 'User logged out successfully'))
        driver_close(self.driver)


if __name__ == '__main__':
    unittest.main()





