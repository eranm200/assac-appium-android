import unittest
from appium import webdriver
from tools.helper import log_in
from tools.helper import assertion_by_line
from tools.helper import sleep_until_app_logged_in
from tools.helper import driver_close
from tools.helper import driver_open
from tools.helper import clear_logs
from tools.helper import desired_caps
from tools.helper import main_port


class AutoLoginTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)

    def test_auto_login(self):
        clear_logs(self.driver)
        driver_open(self.driver)
        sleep_until_app_logged_in()
        driver_close(self.driver)
        clear_logs(self.driver)
        driver_open(self.driver)
        sleep_until_app_logged_in()
        self.assertTrue(assertion_by_line(self.driver, 'App logged in successfully'))


if __name__ == "__main__":
    unittest.main()
