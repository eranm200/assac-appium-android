import unittest
import subprocess
import os
from threading import Thread
from appium import webdriver
from tools.helper import clear_logs
from tools.helper import log_in
from tools.helper import driver_close
from tools.helper import driver_open
from tools.helper import toggle_wifi
from tools.helper import get_first_log
from tools.helper import sleep_until_app_logged_in
from tools.helper import desired_caps
from tools.helper import assertion_by_line
from tools.helper import desired_capsSamsung
from tools.helper import send_message_from_chat_android_to_xiaomi
from tools.helper import call_from_dialer_1006
from tools.helper import answer_call
from tools.helper import hang_up
from tools.helper import assertion_for_missed_call_notification
from tools.helper import assertion_for_message_received_notification
from threading import Thread
import time


class notification_received_Test_ios(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        self.driver2 = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_capsSamsung)
        self.driver.implicitly_wait(5)

    def test_message_notification(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        send_message_from_chat_android_to_xiaomi(self.driver2)
        time.sleep(15)
        self.assertTrue(assertion_by_line(self.driver, "Notification: message was received"))
        driver_close(self.driver)

    def test_missed_call_notification(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        call_from_dialer_1006(self.driver2)
        time.sleep(4)
        time.sleep(15)
        self.assertTrue(assertion_by_line(self.driver, "Notification: missed call"))
        driver_close(self.driver)





if __name__ == '__main__':
    unittest.main()

