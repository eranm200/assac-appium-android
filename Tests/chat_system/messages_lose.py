import unittest
from appium import webdriver
from tools.helper import get_first_log
from tools.helper import clear_logs
from tools.helper import log_in
from tools.helper import driver_close
from tools.helper import assertion_for_message_lost
from tools.helper import sleep_login
from tools.helper import driver_open
from tools.helper import send_message_from_plus_chat_Android
from tools.helper import send_message_from_contacts_iOS
from tools.helper import send_message_chat_Android
from tools.helper import sleep_msg_iOS
from tools.helper import resend_msg_Android
from tools.helper import toggle_wifi
from tools.helper import desired_caps




class Test_message_lose_Android(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        self.driver.implicitly_wait(20)

    def test_message_lose_Android(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        sleep_login(self.driver)
        clear_logs(self.driver)
        send_message_from_contacts_iOS(self.driver, 'iOS')
        sleep_msg_iOS(self.driver)
        self.assertTrue(assertion_for_message_lost(self.driver))
        sleep_msg_iOS(self.driver)







if __name__ == '__main__':
    unittest.main()
