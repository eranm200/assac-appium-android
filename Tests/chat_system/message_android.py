from tools.helper import *
from threading import Thread


class MessageTestAndroid(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver2 = webdriver.Remote(second_port, secondary_desired_caps)
        self.driver.implicitly_wait(5)

    def test_send_msg_from_chat_to_android(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        clear_logs(self.driver)
        send_message_from_chat_android(self.driver)
        sleep_msg_sent_to_android()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an android device'))
        sleep_msg_sent_to_android()
        driver_close(self.driver)

    def test_send_msg_from_contacts_to_android(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        clear_logs(self.driver)
        send_message_from_contacts_android(self.driver)
        time.sleep(4)
        sleep_msg_sent_to_android()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an android device'))
        sleep_msg_sent_to_android()
        driver_close(self.driver)

    def test_send_msg_from_plus_in_chat_to_android(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        clear_logs(self.driver)
        send_message_from_plus_chat_android(self.driver)
        sleep_msg_sent_to_android()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an android device'))
        sleep_msg_sent_to_android()
        driver_close(self.driver)

    def test_send_attachment_to_android(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        send_attachment_android(self.driver)
        time.sleep(15)
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an android device'))
        sleep_msg_sent_to_android()

    def test_resend_msg_to_android(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        time.sleep(5)
        resend_msg_android(self.driver)
        sleep_msg_sent_to_android()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an android device'))
        driver_close(self.driver)

    def test_send_msg_from_recent_calls_to_android(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        sleep_until_app_logged_in()
        call_from_dialer_1002(self.driver)
        time.sleep(5)
        hang_up(self.driver)
        send_message_from_recent_calls_android(self.driver)
        sleep_msg_sent_to_android()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an android device'))
        sleep_msg_sent_to_android()
        driver_close(self.driver)

    def test_send_read_receipt_msg_android(self):
        driver_open(self.driver)
        driver_open(self.driver2)
        clear_logs(self.driver2)
        get_first_log(self.driver2)
        sleep_until_app_logged_in()
        Thread(target=send_message_from_chat_android(self.driver)).start()
        Thread(target=go_to_chat_first_conversation(self.driver2)).start()
        time.sleep(13)
        self.assertTrue(assertion_by_line(self.driver2, "QA: Read receipt send for message"))
        sleep_msg_sent_to_android()
        driver_close(self.driver)
        driver_close(self.driver2)

    # def test_delete_msg_android(self):
    #     driver_open(self.driver)
    #     clear_logs(self.driver)
    #     get_first_log(self.driver)
    #     sleep_until_app_logged_in()
    #     sleep_msg_sent_to_android()
    #     send_message_from_chat_android(self.driver)
    #     time.sleep(3)
    #     driver_back(self.driver)
    #     driver_back(self.driver)
    #     delete_message_from_chat_android(self.driver)
    #     sleep_msg_sent_to_android()
    #     # self.assertTrue(assertion_by_line(self.driver, "QA: Read receipt send for message"))
    #     sleep_msg_sent_to_android()
    #     driver_close(self.driver)
    #     driver_close(self.driver2)

    # def test_delete_attachment_android(self):
    #     driver_open(self.driver)
    #     clear_logs(self.driver)
    #     get_first_log(self.driver)
    #     sleep_until_app_logged_in()
    #     sleep_msg_sent_to_android()
    #     delete_attachment_from_chat_android(self.driver)
    #     sleep_msg_sent_to_android()
    #     print_second_log(self.driver)
    #     # self.assertTrue(assertion_by_line(self.driver, "QA: Read receipt send for message"))
    #     sleep_msg_sent_to_android()
    #     driver_close(self.driver)
    #     driver_close(self.driver2)


if __name__ == '__main__':
    unittest.main()








