from tools.helper import *


class MessageReceivedTestAndroid(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver2 = webdriver.Remote(second_port, secondary_desired_caps)
        self.driver.implicitly_wait(7)

    def test_received_message_from_android(self):
        driver_open(self.driver)
        sleep_until_app_logged_in()
        clear_logs(self.driver)
        Thread(target=go_to_chat(self.driver)).start()
        Thread(target=send_message_from_contacts_android(self.driver2)).start()
        clear_logs(self.driver)
        sleep_msg_received_android()
        self.assertTrue(assertion_by_line(self.driver, 'Omemo message received'))
        sleep_msg_received_android()
        driver_close(self.driver)
        driver_close(self.driver2)

    def test_receive_attachment_from_android(self):
        driver_open(self.driver2)
        driver_open(self.driver)
        clear_logs(self.driver)
        sleep_until_app_logged_in()
        clear_logs(self.driver)
        Thread(target=go_to_chat(self.driver)).start()
        Thread(target=send_attachment_android_s8(self.driver2)).start()
        time.sleep(25)
        self.assertTrue(assertion_by_line(self.driver, 'Notification: message was received'))
        sleep_msg_received_android()
        driver_close(self.driver)

    # def test_received_resent_message_android(self):
    #     driver_open(self.driver)
    #     sleep_until_app_logged_in(self.driver)
    #     clear_logs(self.driver)
    #     go_to_chat(self.driver)
    #     resend_msg_android(self.driver2)
    #     sleep_msg_received_android(self.driver)
    #     self.assertTrue(assertion_for_received_android_msg(self.driver))
    #     sleep_msg_received_android(self.driver)
    #     driver_close(self.driver)


if __name__ == '__main__':
    unittest.main()