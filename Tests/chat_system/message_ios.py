from tools.helper import *


class MessageTestIos(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver.implicitly_wait(30)

    def test_send_msg_from_chat_to_ios(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        send_message_chat_ios(self.driver)
        sleep_msg_sent_to_ios()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an ios device'))
        sleep_msg_sent_to_ios()
        driver_close(self.driver)

    def test_send_msg_from_contacts_to_ios(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        send_message_from_contacts_ios(self.driver, 'iOS')
        sleep_msg_sent_to_ios()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an ios device'))
        sleep_msg_sent_to_ios()
        driver_close(self.driver)

    def test_send_msg_from_plus_in_chat_to_ios(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        send_message_from_plus_chat_ios(self.driver)
        sleep_msg_sent_to_ios()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an ios device'))
        sleep_msg_sent_to_ios()
        driver_close(self.driver)

    def test_send_attachment_to_ios(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        clear_logs(self.driver)
        send_attachmant_ios(self.driver)
        sleep_msg_sent_to_ios()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an ios device'))
        sleep_msg_sent_to_ios()
        driver_close(self.driver)

    def test_resend_msg_to_ios(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        sleep_until_app_logged_in()
        resend_msg_ios(self.driver)
        sleep_msg_sent_to_ios()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an ios device'))

    def test_send_msg_from_recent_calls_to_ios(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        sleep_until_app_logged_in()
        call_from_dialer_9042(self.driver)
        time.sleep(5)
        hang_up(self.driver)
        send_message_from_recent_calls_ios(self.driver)
        sleep_msg_sent_to_ios()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an ios device'))
        sleep_msg_sent_to_ios()
        driver_close(self.driver)


if __name__ == '__main__':
    unittest.main()







