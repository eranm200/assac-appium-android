import unittest
from appium import webdriver
from tools.helper import driver_open
from tools.helper import log_in
from tools.helper import sleep_until_app_logged_in
from tools.helper import clear_logs
from tools.helper import get_first_log
from tools.helper import call_from_dialer_8899
from tools.helper import print_second_log
from tools.helper import toggle_wifi
from tools.helper import assert_for_network_change
from tools.helper import driver_close
from tools.helper import desired_caps
from tools.helper import sleep_call_connecting
from tools.helper import main_port
import time


class NetworkChanges(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver.implicitly_wait(10)

    def test_call_network_change(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        # log_in(self.driver, '1006', '@31!qwsdFg')
        sleep_until_app_logged_in()
        call_from_dialer_8899(self.driver)
        clear_logs(self.driver)
        sleep_call_connecting()
        toggle_wifi(self.driver)
        time.sleep(8)
        toggle_wifi(self.driver)
        time.sleep(8)
        self.assertTrue(assert_for_network_change(self.driver))
        driver_close(self.driver)


if __name__ == '__main__':
    unittest.main()




