import unittest
import time
from appium import webdriver
from threading import Thread
from tools.helper import get_first_log
from tools.helper import clear_logs
from tools.helper import sleep_login
from tools.helper import leave_group
from tools.helper import assertion_for_Android_msg
from tools.helper import assertion_for_attachment
from tools.helper import sleep_msg_Android
from tools.helper import create_group_chat
from tools.helper import send_message_group_chat
from tools.helper import send_attachment_group_chat
from tools.helper import add_contacts_group_chat
from tools.helper import mute_group
from tools.helper import get_second_log
from tools.helper import assertion_for_create_group
from tools.helper import sleep_group_chat_created
from tools.helper import desired_caps
from tools.helper import driver_close
from tools.helper import driver_open
from tools.helper import assertion_for_added_to_group



class group_chat_receive_test(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        self.driver.implicitly_wait(5)

    def test_added_to_Group(self):
        driver_open(self.driver)
        sleep_login(self.driver)
        clear_logs(self.driver)
        self.assertTrue(assertion_for_added_to_group(self.driver))
        driver_close(self.driver)

    # def received_message(self):

    # def received_attachment(self):

    # def removed_from_group(self):


if __name__ == '__main__':
    unittest.main()