from tools.helper import *
from threading import Thread
import time


class GroupChatTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver2 = webdriver.Remote(second_port, secondary_desired_caps)
        self.driver.implicitly_wait(5)

    def test_create_group(self):
        get_first_log(self.driver)
        clear_logs(self.driver)
        sleep_until_app_logged_in()
        create_group_chat(self.driver)
        sleep_group_chat_created()
        self.assertTrue(assertion_by_line(self.driver, 'Group chat was created'))
        driver_close(self.driver)

    def test_send_attachment_in_group(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        send_attachment_in_group_chat(self.driver)
        time.sleep(8)
        self.assertTrue(assertion_by_line(self.driver, 'Attachment sent'))
        sleep_msg_sent_to_android()
        driver_close(self.driver)

    def test_send_msg_in_group_chat(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        send_message_in_group_chat(self.driver)
        time.sleep(8)
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an android device'))
        sleep_msg_sent_to_android()
        driver_close(self.driver)

    def test_mute_group_chat(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        mute_group_chat(self.driver)
        self.assertTrue(assertion_by_line(self.driver, 'group muted true'))
        sleep_group_chat_mute()
        driver_close(self.driver)

    # def test_leave_from_group_chat(self):
    #     driver_open(self.driver)
    #     clear_logs(self.driver)
    #     get_first_log(self.driver)
    #     sleep_until_app_logged_in(self.driver)
    #     leave_group_chat(self.driver)
    #     print_second_log(self.driver)
    #     driver_close(self.driver)
    #     # //add assertion logs for leave//

    def test_send_call_from_group_chat(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        Thread(target=call_contact_from_group_chat(self.driver)).start()
        Thread(target=answer_call(self.driver2)).start()
        sleep_call_connecting()
        self.assertTrue(assertion_by_line(self.driver, 'Call connecting'))
        sleep_call_connecting()
        hang_up(self.driver)
        driver_close(self.driver)

    def test_send_msg_from_group_chat(self):
        driver_open(self.driver)
        clear_logs(self.driver)
        sleep_until_app_logged_in()
        clear_logs(self.driver)
        message_from_group_chat(self.driver)
        sleep_msg_sent_to_android()
        self.assertTrue(assertion_by_line(self.driver, 'Message sent to an android device'))
        sleep_msg_sent_to_android()
        driver_close(self.driver)

    # def test_admin_remove_member_from_group(self):
    #     driver_open(self.driver)
    #     clear_logs(self.driver)
    #     sleep_until_app_logged_in(self.driver)
    #     # /////need to add logs///
    #     admin_remove_member_from_group_chat(self.driver)
    #     time.sleep(5)
    #     print_second_log(self.driver)
    #     driver_close(self.driver)
    #
    # def test_admin_add_Contact_to_group(self):
    #     driver_open(self.driver)
    #     clear_logs(self.driver)
    #     get_first_log(self.driver)
    #     sleep_until_app_logged_in(self.driver)
    #     send_message_in_group_chat(self.driver)
    #     time.sleep(3)
    #     print_second_log(self.driver)
    #     # /////need to add xpath for + add contact///
    #     driver_close(self.driver)


if __name__ == '__main__':
    unittest.main()








