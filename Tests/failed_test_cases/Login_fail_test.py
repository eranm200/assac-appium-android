import unittest
from appium import webdriver
from tools.helper import log_in
from tools.helper import get_first_log
from tools.helper import clear_logs
from tools.helper import assertion_for_login
from tools.helper import sleep_login
from tools.helper import log_out
from tools.helper import driver_open
from tools.helper import assert_for_logout
from tools.helper import desired_caps

class LoginTest(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        self.driver.implicitly_wait(15)

    def test_fail_login(self):
        clear_logs(self.driver)
        get_first_log(self.driver)
        log_in(self.driver, '1007', 'GtyH67*ujh')
        sleep_login(self.driver)
        self.assertTrue(assertion_for_login(self.driver))


if __name__ == '__main__':
    unittest.main()






