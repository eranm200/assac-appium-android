import unittest
import time
from appium import webdriver
from tools.helper import log_in
from tools.helper import get_first_log
from tools.helper import clear_logs
from tools.helper import assertion_by_line
from tools.helper import log_out
from tools.helper import driver_open
from tools.helper import driver_close
from tools.helper import desired_caps
from tools.helper import desired_capsJ3
from tools.helper import sleep_until_app_logged_in
from tools.helper import go_to_shield
from tools.helper import scan_apps
from tools.helper import print_first_log
from tools.helper import print_second_log
from tools.helper import main_port

class AppsSafety(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver.implicitly_wait(6)

    def test_manual_scan(self):
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        scan_apps(self.driver)
        time.sleep(135)
        clear_logs(self.driver)
        print_first_log(self.driver)
        print_second_log(self.driver)
        # self.assertTrue(assertion_by_line(self.driver, "App logged in successfully"))
        driver_close(self.driver)


if __name__ == '__main__':
    unittest.main()
