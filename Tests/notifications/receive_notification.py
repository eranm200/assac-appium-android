import unittest
from tools.helper import *
import subprocess
import os
from threading import Thread
from appium import webdriver
import time


class ReceiveNotificationsTestAndroid(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Remote(main_port, desired_caps)
        self.driver2 = webdriver.Remote(second_port, secondary_desired_caps)
        self.driver.implicitly_wait(5)

    def test_receive_message_notification_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        send_message_from_chat_to_xiaomi(self.driver2)
        time.sleep(12)
        self.assertTrue(assertion_by_line(self.driver, "QA: Notification: message was received"))
        driver_close(self.driver)

    def test_receive_attachment_notification_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        send_attachment_android_s8(self.driver2)
        time.sleep(25)
        self.assertTrue(assertion_by_line(self.driver, "File received successfully"))
        driver_close(self.driver)

    def test_receive_missed_call_notification_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        call_from_dialer_1006(self.driver2)
        time.sleep(20)
        self.assertTrue(assertion_by_line(self.driver, "Notification: missed call"))
        driver_close(self.driver)

    def test_added_to_group_chat_notification_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        create_group_chat(self.driver2)
        time.sleep(17)
        self.assertTrue(assertion_by_line(self.driver, "Notification: You have been added to group chat"))
        driver_close(self.driver)

    def test_receive_message_notification_from_group_chat_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        send_message_in_group_chat(self.driver2)
        time.sleep(20)
        self.assertTrue(assertion_by_line(self.driver, "Notification: Group message was received"))
        driver_close(self.driver)

    def test_receive_kicked_out_of_group_notification_android(self):
        Thread(target=driver_open(self.driver)).start()
        Thread(target=driver_open(self.driver2)).start()
        clear_logs(self.driver)
        get_first_log(self.driver)
        sleep_until_app_logged_in()
        admin_remove_member_from_group_chat(self.driver2)
        time.sleep(20)
        self.assertTrue(assertion_by_line(self.driver, "Notification: You were kicked from group"))
        driver_close(self.driver)


if __name__ == '__main__':
    unittest.main()

